<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'FeedController@index']);
Route::get('/latest-feeds', ['as' => 'latest-feeds', 'uses' => 'FeedController@index']);
Route::get('/feed-category/{feed_category}', ['as' => 'feeds.show.category', 'uses' => 'FeedController@showCategory']);


Route::get('/update', ['as' => 'feeds.update', 'uses' => 'FeedController@updateFeeds']);

Route::get('/login', ['as' => 'auth.login_form', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'backend.dashboard', 'uses' => 'FeedSourceController@index']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('/my-profile', ['as' => 'backend.users.profile-show', 'uses' => 'UserController@editProfile']);
        Route::put('/my-profile/save', ['as' => 'backend.users.profile-save', 'uses' => 'UserController@updateProfile']);
    });

    Route::group(['prefix' => 'feeds'], function () {

        Route::group(['prefix' => 'categories'], function () {
            Route::get('/', ['as' => 'backend.feed-categories.index', 'uses' => 'FeedCategoryController@index']);
            Route::get('/create', ['as' => 'backend.feed-categories.create', 'uses' => 'FeedCategoryController@create']);
            Route::post('/store', ['as' => 'backend.feed-categories.store', 'uses' => 'FeedCategoryController@store']);

            Route::get('/{id}/edit', ['as' => 'backend.feed-categories.edit', 'uses' => 'FeedCategoryController@edit']);
            Route::put('/{id}/update', ['as' => 'backend.feed-categories.update', 'uses' => 'FeedCategoryController@update']);
            Route::get('/{id}/delete', ['as' => 'backend.feed-categories.delete', 'uses' => 'FeedCategoryController@delete']);
        });

        Route::group(['prefix' => 'feed-urls'], function () {
            Route::get('/', ['as' => 'backend.feed-source.index', 'uses' => 'FeedSourceController@index']);
            Route::get('/create', ['as' => 'backend.feed-source.create', 'uses' => 'FeedSourceController@create']);
            Route::post('/store', ['as' => 'backend.feed-source.store', 'uses' => 'FeedSourceController@store']);
            Route::get('/{id}/edit', ['as' => 'backend.feed-source.edit', 'uses' => 'FeedSourceController@edit']);
            Route::put('/{id}/update', ['as' => 'backend.feed-source.update', 'uses' => 'FeedSourceController@update']);
            Route::get('/{id}/delete/', ['as' => 'backend.feed-source.delete', 'uses' => 'FeedSourceController@delete']);
        });
    });
});