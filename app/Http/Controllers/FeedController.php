<?php

namespace App\Http\Controllers;

use App\Repositories\FeedCategoryRepository;
use App\Repositories\FeedRepository;
use App\Repositories\FeedSourceRepository;
use App\Services\FeedService;

class FeedController extends Controller
{
    private $feeds;
    private $sources;
    private $feed_categories;

    public function __construct(FeedRepository $feeds, FeedSourceRepository $sources, FeedCategoryRepository $feed_categories)
    {
        $this->feeds = $feeds;
        $this->sources = $sources;
        $this->feed_categories = $feed_categories;
    }

    public function index()
    {
        $view_data = [
            'feeds'      => $this->feeds->latest()->paginate(20),
            'categories' => $this->feed_categories->all(),
        ];

        return view('home', $view_data);
    }

    public function showCategory($feed_category)
    {
        $feeds = $feed_category->feeds()->latest()->paginate(20);

        $view_data = [
            'feeds'      => $feeds,
            'categories' => $this->feed_categories->all(),
        ];

        return view('home', $view_data);
    }

    public function updateFeeds()
    {
        $urls = $this->sources->get()->pluck('url', 'id');

        // dd($urls);

        $this->feeds->truncate();

        foreach ($urls as $source_id => $url) {
            $feed_file = file_get_contents($url);


            $feed_xml = simplexml_load_string($feed_file);

            if ($feed_xml['version'] != '2.0') {
                dd('Version Error!');
            }

            foreach ($feed_xml->channel->item as $item) {
                $feed = $this->extractItem($item);
                $feed['source_id'] = $source_id;
                $feed['channel_link'] = (string)$feed_xml->channel->link;

                $this->feeds->firstOrCreate($feed);
                // dd($item->children());
                // dump( (string)$item->title);
                // echo '<a href="' . $item->link .'">' . $item->title . '</a><br>';
            }

            echo '<br>======<br>';

            // dd($provider_feeds->channel->item);
        }
        // $this->feeds->save();
        // dd($urls);
    }

    private function extractItem($item)
    {
        return [
            'title'   => (string)$item->title,
            'guid'    => (string)$item->guid,
            'link'    => (string)$item->link,
            'pub_date' => (string)$item->pubDate,
        ];
    }
}
