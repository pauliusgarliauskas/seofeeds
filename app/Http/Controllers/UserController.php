<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Repositories\CurrentUserRepository;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $current_user;

    public function __construct(CurrentUserRepository $current_user)
    {
        $this->current_user = $current_user;
    }
    public function editProfile()
    {
        return view('users.edit');
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $new_password = $request->input('password');
        $this->current_user->setPassword($new_password);

        return redirect()->route('backend.users.profile-show')->with('alert.success', 'Password updated!');
    }
}
