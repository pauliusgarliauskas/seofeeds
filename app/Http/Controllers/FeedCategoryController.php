<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedCategoryRequest;
use App\Repositories\FeedCategoryRepository;

class FeedCategoryController extends Controller
{
    private $feed_categories;

    public function __construct(FeedCategoryRepository $feed_categories)
    {
        $this->feed_categories = $feed_categories;
    }

    public function index()
    {

        $view_data = [
            'categories' => $this->feed_categories->root()->with('children')->get(),
        ];

        return view('feeds.categories.index', $view_data);
    }

    public function create()
    {
        $view_data = [
            'available_parents' => $this->feed_categories->all(),
        ];

        return view('feeds.categories.create', $view_data);
    }

    public function store(FeedCategoryRequest $request)
    {
        $this->feed_categories->create($request->all());

        return redirect()->route('backend.feed-categories.index')->with('alert.success', 'Category created!');
    }

    public function edit($id)
    {
        $category = $this->feed_categories->findOrFail($id);

        $available_parents = $this->feed_categories->with('children')->where('id', '<>', $id)->get();

        $view_data = [
            'category'          => $category,
            'available_parents' => $available_parents,
        ];

        return view('feeds.categories.edit', $view_data);
    }

    public function update(FeedCategoryRequest $request, $id)
    {
        $category = $this->feed_categories->findOrFail($id);

        $category->update($request->all());

        return redirect()->route('backend.feed-categories.edit', $category->id)
            ->with('alert.success', 'Category updated!');
    }

    public function delete($id)
    {
        $category = $this->feed_categories->findOrFail($id);

        $category->delete();

        return redirect()->route('backend.feed-categories.index')->with('alert.success', 'Category deleted!');
    }
}
