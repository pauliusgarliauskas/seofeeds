<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedSourceRequest;
use App\Repositories\FeedCategoryRepository;
use App\Repositories\FeedSourceRepository;

class FeedSourceController extends Controller
{
    private $feed_sources;
    private $feed_categories;

    public function __construct(FeedSourceRepository $feed_sources, FeedCategoryRepository $feed_categories)
    {
        $this->feed_sources = $feed_sources;
        $this->feed_categories = $feed_categories;
    }

    public function index()
    {
        $view_data = [
            'feed_sources' => $this->feed_sources->with('category')->get(),
        ];

        return view('feeds.sources.index', $view_data);
    }

    public function create()
    {
        $categories = $this->feed_categories->get()->pluck('name', 'id');

        $view_data = [
            'categories' => $categories,
        ];

        return view('feeds.sources.create', $view_data);
    }

    public function store(FeedSourceRequest $request)
    {
        $this->feed_sources->create($request->all());

        return redirect()->route('backend.feed-source.index')->with('alert.success', 'Feed source created!');
    }

    public function edit($id)
    {
        $source = $this->feed_sources->findOrFail($id);

        $categories = $this->feed_categories->get()->pluck('name', 'id');

        $view_data = [
            'source'     => $source,
            'categories' => $categories,
        ];

        return view('feeds.sources.edit', $view_data);

    }

    public function update(FeedSourceRequest $request, $id)
    {
        $source = $this->feed_sources->findOrFail($id);

        $source->fill($request->all());
        $source->save();

        return redirect()->route('backend.feed-source.index')->with('alert.success', 'Feed source updated!');
    }

    public function delete($id)
    {
        $source = $this->feed_sources->findOrFail($id);
        $source->delete();

        return redirect()->route('backend.feed-source.index')->with('alert.success', 'Feed source deleted!');
    }
}
