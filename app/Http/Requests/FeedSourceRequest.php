<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedSourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->getMethod();

        if($method == 'PUT') {
            return [
                'category_id' => 'required|exists:feed_categories,id',
                'url'      => 'required|url|unique:feed_sources,url,' . $this->id,
            ];
        }

        return [
            'category_id' => 'required|exists:feed_categories,id',
            'url'      => 'required|url|unique:feed_sources,url',
        ];
    }
}
