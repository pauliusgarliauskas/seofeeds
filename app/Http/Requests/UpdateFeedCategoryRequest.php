<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFeedCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        dd($this->getMethod());
        return [
            'parent_id' => 'exists:feed_categories,id',
            'name' => 'required',
            'slug' => 'required|alpha_dash|unique:feed_categories,slug,id',
        ];
    }
}
