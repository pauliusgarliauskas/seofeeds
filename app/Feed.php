<?php

namespace App;

use App\Repositories\FeedRepository;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model implements FeedRepository
{
    protected $fillable = [
        'source_id',
        'channel_link',
        'title',
        'guid',
        'link',
        'pub_date',
    ];

    /**
     * Latest feeds, by published date
     *
     * @return mixed
     */
    public function latest()
    {
        return parent::latest('pub_date');
    }

    /**
     * On set, convert publish date to mysql compatible timestamp
     *
     * @param $value
     */
    public function setPubDateAttribute($value)
    {
        $this->attributes['pub_date'] = date("Y-m-d H:i:s", strtotime((string)$value));
    }

    /**
     * Return pub_time attribute in year-month-day hour:minutes format
     *
     * @return false|string
     */
    public function getPubDateAttribute()
    {
        return date('Y-m-d H:i', strtotime($this->attributes['pub_date']));
    }
}
