<?php

namespace App;

use App\Repositories\FeedCategoryRepository;
use Illuminate\Database\Eloquent\Model;

class FeedCategory extends Model implements FeedCategoryRepository
{
    protected $table = 'feed_categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
    ];

    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = trim($value) !== '' ? $value : null;
    }

    /**
     * Scope to select root categories
     *
     * @param $query
     * @return mixed
     */
    public function scopeRoot($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Scope to filter by slug
     *
     * @param $query
     * @param $slug
     * @return mixed
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    /**
     * ORM. Child categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\FeedCategory', 'parent_id', 'id');
    }

    /**
     * ORM. Parent category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\FeedCategory', 'id', 'parent_id');
    }

    /**
     * ORM. Feeds, related to category. Relates through source model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function feeds()
    {
        return $this->hasManyThrough('App\Feed', 'App\FeedSource', 'category_id', 'source_id');
    }
}
