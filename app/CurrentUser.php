<?php

namespace App;

use App\Repositories\CurrentUserRepository;
use Illuminate\Support\Facades\Auth;

class CurrentUser implements CurrentUserRepository
{
    /**
     * Set password for currently active user
     *
     * @param $password
     */
    public function setPassword($password)
    {
        /* Get active user*/
        $user = Auth::user();

        /* Hash password string, assign to user and save */
        $user->password = \Hash::make($password);
        $user->save();
    }
}
