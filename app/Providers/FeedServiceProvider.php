<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class FeedServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        /* Bind feed category to route */
        Route::bind('feed_category', function ($value) {
            return \App\FeedCategory::where('slug', $value)->first();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /* Bind repositories */
        $this->app->bind('App\Repositories\FeedCategoryRepository', 'App\FeedCategory');
        $this->app->bind('App\Repositories\FeedSourceRepository', 'App\FeedSource');
        $this->app->bind('App\Repositories\FeedRepository', 'App\Feed');
    }
}
