<?php

namespace App\Console\Commands;

use App\Services\FeedUpdateService;
use Illuminate\Console\Command;

class FeedsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feeds:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Removing old feeds');
        FeedUpdateService::removeOld();

        $this->info('Updating feeds');
        FeedUpdateService::updateFeeds();
    }
}
