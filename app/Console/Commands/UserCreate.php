<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* Get credentials */
        $first_name = $this->ask('Enter your first name');
        $last_name = $this->ask('Enter your last name');
        $username = $this->ask('Enter login name');
        $password = $this->secret('Enter password');

        /* Create user */
        User::create([
            'first_name' => $first_name,
            'last_name'  => $last_name,
            'username'   => $username,
            'password'   => \Hash::make($password),
        ]);
    }
}
