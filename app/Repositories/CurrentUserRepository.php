<?php
/**
 * Created by PhpStorm.
 * User: paulius
 * Date: 12/18/16
 * Time: 10:15 PM
 */

namespace App\Repositories;


interface CurrentUserRepository
{
    public function setPassword($password);
}