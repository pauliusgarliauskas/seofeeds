<?php
/**
 * Created by PhpStorm.
 * User: paulius
 * Date: 12/12/16
 * Time: 8:04 PM
 */

namespace App\Repositories;


interface FeedSourceRepository
{
    public function category();
}