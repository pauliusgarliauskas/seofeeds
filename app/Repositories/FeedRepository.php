<?php
/**
 * Created by PhpStorm.
 * User: paulius
 * Date: 12/12/16
 * Time: 9:41 PM
 */

namespace App\Repositories;


interface FeedRepository
{
    public function latest();
}