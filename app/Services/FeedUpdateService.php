<?php

namespace App\Services;

use App\Feed;
use App\FeedSource;
use App\Providers\FeedServiceProvider;

class FeedUpdateService
{
    public static function removeOld()
    {
        Feed::truncate();
    }

    /**
     * Update feeds
     */
    public static function updateFeeds()
    {
        /* Get urls from database */
        $urls = FeedSource::get()->pluck('url', 'id');

        /* Loop urls */
        foreach ($urls as $source_id => $url) {
            /* Get feed file */
            $feed_file = file_get_contents($url);

            /* Convert to xmo */
            $feed_xml = simplexml_load_string($feed_file);

            /* Read feeds and put them to database */
            foreach ($feed_xml->channel->item as $item) {
                $feed = self::extractItem($item);
                $feed['source_id'] = $source_id;
                $feed['channel_link'] = (string)$feed_xml->channel->link;

                Feed::create($feed);
            }
        }
    }

    /**
     * Extract data, which is needed for database, from item
     *
     * @param $item
     * @return array
     */
    private static function extractItem($item)
    {
        return [
            'title'   => (string)$item->title,
            'guid'    => (string)$item->guid,
            'link'    => (string)$item->link,
            'pub_date' => (string)$item->pubDate,
        ];
    }
}