<?php

namespace App;

use App\Repositories\FeedSourceRepository;
use Illuminate\Database\Eloquent\Model;

class FeedSource extends Model implements FeedSourceRepository
{
    protected $table = 'feed_sources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'category_id',
        'url',
    ];

    /**
     * ORM. Category in which source is in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\FeedCategory', 'category_id', 'id');
    }
}
