@extends('layouts.dashboard')


@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Change my password</h3>
        </div>
        <div class="panel-body">
            <form action="{{ route('backend.users.profile-save') }}" method="POST">
                <div class="form-group @if($errors->has('password')) has-error @endif">
                    <input type="password" name="password" value="" class="form-control" placeholder="Password">
                    @if($errors->has('password'))
                        <span class="control-label">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <div class="form-group @if($errors->has('password')) has-error @endif">
                    <input type="password" name="password_confirmation" value="" class="form-control" placeholder="Confirm password">
                    @if($errors->has('password'))
                        <span class="control-label">{{ $errors->first('url') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-primary">
                    <a href="#" onclick="window.history.back()" class="btn btn-success">Cancel</a>
                </div>

                <input type="hidden" name="_method" value="PUT">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@stop