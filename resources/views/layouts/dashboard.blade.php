<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="robots" content="noindex, nofollow">

    <title>
        @yield('page-title')
    </title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">

</head>
<body>
<div class="navbar navbar-default">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('backend.dashboard') }}">Dashboard</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        Feeds
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('backend.feed-categories.index') }}">Categories</a></li>
                        <li><a href="{{ route('backend.feed-source.index') }}">Urls</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('home') }}" target="_blank">View site</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <span class="glyphicon glyphicon-user"></span> {{ $active_user->full_name }}
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('backend.users.profile-show') }}">Edit profile</a></li>
                        <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</div>

@if(\Session::has('alert'))
    <div class="system-alerts">
        <div class="container">
            @foreach(\Session::get('alert') as $type => $msg)
                <div class="alert alert-{{ $type }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $msg }}
                </div>
            @endforeach
        </div>
    </div>
@endif

<div class="container">
    @yield('content')
</div>

<script src="{{ asset('/assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>

</body>
</html>