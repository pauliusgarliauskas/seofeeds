<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="robots" content="noindex, nofollow">

    <title>
        @yield('page-title')
    </title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">

</head>
<body>
<div class="navbar navbar-default">
    <div class="container">

        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('home') }}">SeoFeeds</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('latest-feeds') }}">Latest feeds</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    @if(Auth::guest())
                        <a href="{{ route('auth.login') }}">Login</a>
                    @else
                        <a href="{{ route('backend.dashboard') }}" target="_blank">Dashboard</a>
                    @endif
                </li>
            </ul>
        </div>

    </div>
</div>

@if(\Session::has('alert'))
    <div class="system-alerts">
        <div class="container">
            @foreach(\Session::get('alert') as $type => $msg)
                <div class="alert alert-{{ $type }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ $msg }}
                </div>
            @endforeach
        </div>
    </div>
@endif

<div class="container">
    @yield('content')
</div>

<script src="{{ asset('/assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>

@yield('scripts')

</body>
</html>