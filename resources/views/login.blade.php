<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="robots" content="noindex, nofollow">

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">

    <title>Login</title>
</head>
<body>

<div id="login-form">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Login
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('auth.login') }}" method="POST" class="form">
                            <div class="form-group @if($errors->has('username')) has-error @endif">
                                <input type="text" class="form-control" name="username" placeholder="Username">
                                @if($errors->has('username'))
                                    <span class="control-label">{{ $errors->first('username') }}</span>
                                @endif
                            </div>

                            <div class="form-group @if($errors->has('password')) has-error @endif">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                @if($errors->has('password'))
                                    <span class="control-label">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="form-group text-right">
                                <input type="submit" class="btn btn-primary" value="Login">
                            </div>

                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>

</body>
</html>
