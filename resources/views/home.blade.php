@extends('layouts.main')

@section('page-title')
    Home
@stop

@section('content')
    <div class="row">

        <div class="col-md-3">
            <h3>
                Categories
            </h3>

            <div>
                @foreach($categories as $category)
                    <a href="{{ route('feeds.show.category', $category->slug) }}">
                        {{ $category->name }}
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-md-9">
            <h3>Latest feeds</h3>
            <table class="table table-hover">
                @foreach($feeds as $feed)
                    <tr>
                        <td>
                            <a href="{{ $feed->link }}" data-toggle="modal" data-target="#open-feed-modal">
                                {{ $feed->title }}
                            </a>
                        </td>
                        <td class="text-right">
                            <small>{{ $feed->pub_date }}</small>
                            <span class="small">
                                (<a href="{{ $feed->channel_link }}" target="_blank">Author</a>)
                            </span>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $feeds->links() }}
        </div>
    </div>

    <div id="open-feed-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" target="_blank" class="feed-link btn btn-primary btn-block">Go to feed page</a>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        'use strict'

        $('#open-feed-modal').on('show.bs.modal', function (event) {
            var caller = $(event.relatedTarget);
            var link = caller.attr('href');

            $(this).find('.feed-link').attr('href', link);
        })
    </script>
@append