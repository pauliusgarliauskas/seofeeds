@extends('layouts.dashboard')

@section('page-title')
    Feed groups
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Feed categories</h3>
        </div>
        <div class="panel-body">
            <a class="btn btn-primary pull-right" href="{{ route('backend.feed-categories.create') }}">Add category</a>
            <ul>
                @each('feeds.categories.partials.category', $categories, 'category')
            </ul>
        </div>
    </div>
@stop
