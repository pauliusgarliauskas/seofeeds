@extends('layouts.dashboard')

@section('page-title')
    Feed groups | create
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create feed category</h3>
        </div>

        <div class="panel-body">
            <form action="{{ route('backend.feed-categories.store') }}" method="POST">
                <div class="form-group @if($errors->has('parent_id')) has-error @endif">
                    <label for="parent_id">
                        Parent
                    </label>
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option value="">Root</option>
                        @foreach($available_parents as $parent)
                            <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('parent_id'))
                        <span class="control-label">{{ $errors->first('parent_id') }}</span>
                    @endif
                </div>

                <div class="form-group @if($errors->has('name')) has-error @endif">
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Title">
                    @if($errors->has('name'))
                        <span class="control-label">{{ $errors->first('name') }}</span>
                    @endif
                </div>

                <div class="form-group @if($errors->has('slug')) has-error @endif">
                    <input type="text" name="slug" value="{{ old('slug') }}" class="form-control" placeholder="Slug">
                    @if($errors->has('slug'))
                        <span class="control-label">{{ $errors->first('slug') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                    <a href="{{ route('backend.feed-categories.index') }}" class="btn btn-success">Cancel</a>
                </div>

                {{ csrf_field() }}
            </form>
        </div>
    </div>
@stop