<li>
    <a href="{{ route('backend.feed-categories.edit', $category->id) }}">
        {{ $category->name }}
    </a>


    @if( $category->children )
        <ul>
            @each('feeds.categories.partials.category', $category->children, 'category')
        </ul>
    @endif
</li>