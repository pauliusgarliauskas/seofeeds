@extends('layouts.dashboard')

@section('page-title')
    Feed groups | create
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Add feed source</h3>
        </div>

        <div class="panel-body">
            <form action="{{ route('backend.feed-source.store') }}" method="POST">

                <div class="form-group @if($errors->has('category_id')) has-error @endif">
                    <select name="category_id" class="form-control">
                        @foreach($categories as $id => $name)
                            <option value="{{ $id }}" @if(old('category_id') == $id) selected @endif >
                                {{ $name }}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has('category_id'))
                        <span class="control-label">{{ $errors->first('category_id') }}</span>
                    @endif
                </div>

                <div class="form-group @if($errors->has('url')) has-error @endif">
                    <input type="text" name="url" value="{{ old('url') }}" class="form-control" placeholder="Url">
                    @if($errors->has('url'))
                        <span class="control-label">{{ $errors->first('url') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save">
                </div>

                {{ csrf_field() }}
            </form>
        </div>
    </div>
@stop