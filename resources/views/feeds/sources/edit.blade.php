@extends('layouts.dashboard')

@section('page-title')
    Feed groups | edit
@stop

@section('content')
    <form action="{{ route('backend.feed-source.update', $source->id) }}" method="POST">

        <div class="form-group @if($errors->has('category_id')) has-error @endif">
            <select name="category_id" class="form-control">
                @foreach($categories as $id => $name)
                    <option value="{{ $id }}" @if(old('category_id', $source->category_id) == $id) selected @endif >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            @if($errors->has('category_id'))
                <span class="control-label">{{ $errors->first('category_id') }}</span>
            @endif
        </div>

        <div class="form-group @if($errors->has('url')) has-error @endif">
            <input type="text" name="url" value="{{ old('url', $source->url) }}" class="form-control" placeholder="Url">
            @if($errors->has('url'))
                <span class="control-label">{{ $errors->first('url') }}</span>
            @endif
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Save">
            <a href="#" onclick="window.history.back()" class="btn btn-success">Cancel</a>
        </div>

        <input type="hidden" name="id" value="{{ $source->id }}">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

    </form>
@stop