@extends('layouts.dashboard')

@section('page-title')
    Feed urls
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Feed sources</h3>
        </div>

        <div class="panel-body">
            <a class="btn btn-primary pull-right" href="{{ route('backend.feed-source.create') }}">Add Url</a>

            <div class="clearfix"></div>

            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Group</th>
                    <th>Url</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($feed_sources as $source)
                    <tr>
                        <td>{{ $source->id }}</td>
                        <td>
                            <a href="{{ route('backend.feed-categories.edit', $source->category->id) }}">
                                {{ $source->category->name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('backend.feed-source.edit', $source->id) }}">
                                {{ $source->url }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('backend.feed-source.edit', $source->id) }}">
                                Edit
                            </a>
                            <a href="{{ route('backend.feed-source.delete', $source->id) }}">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
