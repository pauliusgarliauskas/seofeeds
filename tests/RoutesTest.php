<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoutesTest extends TestCase
{
    /**
     * Check if can access dashboard without login
     *
     * @return void
     */
    public function testDashboardAccessNotLogged()
    {
        $this->visit('/dashboard')
            ->seePageIs('/login');
    }
}
